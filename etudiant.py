class Etudiant(object):

    # Constructeur de la classe Etudiant
    def __init__(self, nom, prenom, age):
        self.nom = nom
        self.prenom = prenom
        self.age = age
        self.notes = []

    def ajoutNote(self, note):
        self.notes.append(note)     # en PHP: $this->notes

    # La fonction __repr__ permet ensuite d'utiliser "print" avec cette classe
    def __repr__(self):
        res = self.prenom + " " + self.nom + " (" + str(self.age) + " ans) - Notes: "
        for val in self.notes:
            res += str(val) + " "
        return res + "- Moyenne: " + str(self.moyenne())

    def moyenne(self):
        s = 0
        for val in self.notes:
            s += val
        return s / len(self.notes)


def main():
    mark = Etudiant("Zuckerberg", "Mark", 34)
    mark.ajoutNote(12.5)
    mark.ajoutNote(14)
    print(mark)
    bill = Etudiant("Gates", "Bill", 64)
    bill.ajoutNote(13)
    print(bill)


"""
    On vérifie que c'est bien la fonction main() du fichier etudiant.py
    que l'utilisateur souhaite exécuter.
    Ceci permet de tester le fonctionnement de la classe avant de l'utiliser
    dans le fichier main.py
"""
if __name__ == "__main__":
    main()

