from etudiant import Etudiant
from readfile import Readfile
from writefile import WriteFile

"""
    Avec la ligne ci-dessus on récupère la définition de la classe Etudiant
    depuis le fichier etudiant.py dans le même dossier
"""


def check_input_str(str, default='data.txt'):
    # Vérifie que la chaine saisie est clean
    if len(str.strip()) == 0:
        return default.strip()
    else:
        return str.strip()


def print_main_menu():
    list = [
        "1. Ajouter un fichier",
        "2. Ajouter un etudiant",
        "3. Ajouter une note",
        "4. Afficher les notes d'un étudiant",
        "5. Afficher les notes triees d'un cours",
        "6. Supprimer un cours",
        "7. Sauvegarde des données dans un fichier",
        "8. Quitter",
    ]
    for item in list:
        print(item)


def main():

    # etudiants = []
    current_file = object

    while True:
        print_main_menu()
        choix = int(input("Votre choix: "))

        # AJOUER UN FICHIER
        if choix == 1:
            input_file = input("Entrez le nom de votre fichier (ex. data.txt)")
            input_file = check_input_str(input_file, 'data.txt')

            current_file = Readfile(input_file)
            current_file.parse()
            print(current_file)

        # AJOUER UN ETU
        if choix == 2:
            nom = input("Nom: ")
            prenom = input("Prenom: ")
            age = int(input("Age: "))
            etu = Etudiant(nom, prenom, age)

            # charge le fichier de données au besoin
            if not isinstance(current_file, Readfile):
                current_file = Readfile('data.txt')
                current_file.parse()

            current_file.etu.append([etu.nom, etu.prenom, etu.age])
            print(current_file)
            # etudiants.append(etu)

        # AJOUTER UNE NOTE
        elif choix == 3:
            # charge le fichier de données au besoin
            if not isinstance(current_file, Readfile):
                current_file = Readfile('data.txt')
                current_file.parse()

            note = int(input("Note à attribuer : "))
            print('liste des étudiants : ')
            print(current_file.print_students())
            student = int(input('Votre choix : '))

            print('Liste des cours : ')
            print(current_file.print_courses())
            cours = int(input('Votre choix : '))

            current_file.notes.append([note, student, cours])

            print('La note à bien été ajoutée')  # message à retravailer

        # AFFICHER LES NOTES D'UN ETU
        elif choix == 4:
            # charge le fichier de données au besoin
            if not isinstance(current_file, Readfile):
                current_file = Readfile('data.txt')
                current_file.parse()

            print("Choix de l'étudiant : ")
            print(current_file.print_students())
            student = int(input('Votre choix : '))

            print(current_file.print_scores_student(student))

        # AFFICHER LA LISTE DES COURS
        elif choix == 5:
            # charge le fichier de données au besoin
            if not isinstance(current_file, Readfile):
                current_file = Readfile('data.txt')
                current_file.parse()

            print('Liste des cours : ')
            print(current_file.print_courses())
            cours = int(input('Votre choix : '))
            print(current_file.print_scores_course(cours))

        # SUPPRIME UN COURS ET LES NOTES ASSOCIEES
        elif choix == 6:
            # charge le fichier de données au besoin
            if not isinstance(current_file, Readfile):
                current_file = Readfile('data.txt')
                current_file.parse()

            print('Liste des cours : ')
            print(current_file.print_courses())
            cours = int(input('Votre choix : '))
            print(current_file.del_course(cours))

        # SAUVEGARDE UN FICHIER DE DONNEES
        elif choix == 7:
            # charge le fichier de données au besoin
            if not isinstance(current_file, Readfile):
                current_file = Readfile('data.txt')
                current_file.parse()

            new_filename = input("Entrez le nom du fichier : ")
            new_filename = check_input_str(new_filename, 'out.txt')

            new_file = WriteFile(new_filename, current_file.etu, current_file.cours, current_file.notes)
            new_file.write()
            print('Les donnees ont ete sauvegardees')

        # QUITTE LE PROGRAMME
        elif choix == 8:
            quit()


"""
    On vérifie que c'est bien la fonction main() du fichier main.py
    que l'utilisateur souhaite exécuter
"""
if __name__ == "__main__":
    main()