from etudiant import Etudiant


class Readfile(object):

    """Une class qui permet de lire un fichier et de le parser"""

    def __init__(self, file):
        self.file = file
        self.etu = []
        self.cours = []
        self.notes = []

    def parse(self):
        my_file = open(self.file, "r")

        """
        compte la section du fichier :
            0 = etudiant
            1 = cours
            2 = notes
        """
        section = 0

        # tant que le curseur n'a pas atteind la fin du fichier
        while True:
            line = my_file.readline()
            if line == "":
                break
            else:
                decompte = int(line)
                for i in range(decompte):
                    if i >= decompte:
                        break
                    else:
                        self.add_data_to_list(section, my_file.readline())
                # on passe a la section suivante
                section += 1

    # etudiants : [nom, prenom, age]
    def students_list(self, line):
        self.etu.append(line.strip().split(' '))

    # cours : le nom du cours doit etre en un seul mot, [nom, annee]
    def courses_list(self, line):
        self.cours.append(line.strip().split(' '))

    # notes : [note, ID etu, ID cours]
    def notes_list(self, line):
        tmp = line.strip().split(' ')
        note = int(tmp[0])
        etu_id = int(tmp[1])
        cours_id = int(tmp[2])
        self.notes.append([note, etu_id, cours_id])

    def add_data_to_list(self, section, line):
        if section == 0:
            self.students_list(line)
        elif section == 1:
            self.courses_list(line)
        else:
            self.notes_list(line)

    def print_students(self):
        students = ''
        for person in self.etu:
            students += '%s. %s %s (%s ans) \n' % (self.etu.index(person), person[0], person[1], person[2])
        return students

    def print_courses(self):
        courses = ''
        for cours in self.cours:
            courses += '%s. %s (%s) \n' % (self.cours.index(cours), cours[0], cours[1])
        return courses

    def print_scores_student(self, etu_id):
        scores = 'Les notes de %s %s (%s ans) : \n' % (self.etu[etu_id][0], self.etu[etu_id][1], self.etu[etu_id][2])
        for score in self.notes:
            if score[1] == etu_id:
                scores += '%s (%s) : %s \n' % (self.cours[score[2]][0], self.cours[score[2]][1], score[0])
        return scores

    def print_scores_course(self, cours_id):
        scores = 'Notes triees du cours %s (%s): \n' % (self.cours[cours_id][0], self.cours[cours_id][1])
        sorted_scores = self.notes
        sorted_scores.sort(reverse=True)
        for score in sorted_scores:
            if score[2] == cours_id:
                scores += '%s %s (%s ans) : %s \n' % (self.etu[score[1]][0], self.etu[score[1]][1], self.etu[score[1]][2], score[0])
        return scores

    def del_course(self, cours_id):
        for score in self.notes:
            if score[2] == cours_id:
                del score
        tmp = (self.cours.pop(cours_id))
        return "Le cours %s (%s) et les notes associees ont ete supprimees" % (tmp[0], tmp[1])

    def __repr__(self):
        resp = '%s étudiants \n' % (len(self.etu))
        for person in self.etu:
            resp += '%s %s (%s ans) \n' % (person[0], person[1], person[2])
        resp += '%s cours \n' % (len(self.cours))
        for cour in self.cours:
            resp += '%s (%s) \n' % (cour[0], cour[1])
        resp += '%s notes \n' % (len(self.notes))
        for note in self.notes:
            person = self.etu[note[1]][0] + ' ' + self.etu[note[1]][1]
            age = self.etu[note[1]][2]
            cours = self.cours[note[2]][0] + ' (' + self.cours[note[2]][1] + ')'
            resp += "la note %s est attribué pour l'étudiant %s (%s ans) dans le cours %s \n" % (note[0], person, age, cours)
        return resp
