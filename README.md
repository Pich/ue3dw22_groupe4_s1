# Rapport mini projet semaine 2

## Organisation est fonctionnement

Nous avons utilisé deux outils pour organiser notre travail sur ce projet :

- Discord pour nos échanges synchrones et asynchrones
- Framagit pour le versionning de développement.

Compte tenu du départ de [Pierre Brissaut](https://cvtic.unilim.fr/mod/forum/discuss.php?d=42276) la collaboration autour de ce projet a été simplifié. En effet, Alexandre et moi avons l'habitude travailler ensemble. Nous avons donc décidé de réaliser chacun de notre coté le tutoriel Codecademy de la semaine afin d'avoir une bonne vue d'ensemble sur le travail à réaliser en groupe. De ce fait nous avons décider de consacrer la journée de vendredi à un travail de *Pair programing* autour du projet. 

## Travail réalisé

L'ensemble des fonctionnalités demandés dans l'exercice ont pu être réalisé et son fonctionnelles.

### 1. Ajouter un fichier

![step_1](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_1.png)

### 2. Ajouter un étudiant

![step_2](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_2.png)

### 3. Ajouter une note

![step_3](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_3.png)

### 4. Afficher les notes d'un étudiant

![step_4](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_4.png)

### 5.afficher les notes triées d'un cours

![step_5](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_5.png)

### 6. Supprimer un cours

![step_6](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_6.png)

### 7. Sauvegarder des donnés dans un fichier

![step_7](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_7.png)

Résultat dans le fichier `data.txt` :

![step_7.1](D:\Code\Laragon\Python_S1\ue3dw22_groupe4_s1\step_7.1.png)

## Organisation de nos fichiers

- `etudiant.py` : Fichier comprenant la classe `Etudiant` et ses méthodes associés.
- `main.py`: Fichier central d'exécution de l'appli et point d'entrée il exploite les classes `Etudiant`; `Readfile`, `Writefile`
- `readfile.py`: Fichier comprenant la classe `Readfile` et les méthodes associés.
- `writefile.py`: Fichier comprenant la classe `Writefile` et les méthodes associés.

## Conclusion général

Ce projet nous a permis de commencer à travailler avec des classes en Python et d'entrevoir les possibilités de ce langage lorsqu'il est utilisé en orienté objet. Nous avons pu aussi constater que Python est assez facile à prendre main puisqu'on nous avons pu, en peu de temps ,développer l'ensemble des fonctionnalités demandé et travailler sur des classes, sans avoir une connaissance poussé du langage. 

Cependant nous aurions aimé avoir plus de temps pour affiner l'aspect POO et implémenter la gestion des cas non conforme avec différent retour de message en cas de mauvaise saisie. De plus, nous aurions aimer avoir à traiter un format de fichier (data.txt) plus "pratique" en exploitant des objets JSON, par exemple.

## Conclusion personnel (Vincent)

D'un point de vue professionnel ma semaine a été très chargé et le chevauchement de trois UE ne m'a pas permis d'appréhender ce projet sereinement. Heureusement la collaboration avec Alexandre c'est très bien passé et il a été d'une grande aide pour m'aider à comprendre la logique des classes dans Python.

​	

