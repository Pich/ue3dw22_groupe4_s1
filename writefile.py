class WriteFile(object):

    """Ecrit un nouveau fichier avec les données courantes"""

    def __init__(self, filename, students, courses, grades):
        self.filename = filename
        self.students = students
        self.courses = courses
        self.grades = grades

    def write(self):
        f = open(self.filename, "w")
        # students roster
        f.write(self.section_header(self.students))
        for student in self.students:
            f.write(self.roster(student))
        # courses lists
        f.write(self.section_header(self.courses))
        for course in self.courses:
            f.write(self.curriculum(course))
        # grades matrix
        f.write(self.section_header(self.grades))
        for grade in self.grades:
            f.write(self.scoresheet(grade))
        # EOF
        f.close

    def scoresheet(self, data):
        grade = "%s %s %s" % (data[0], data[1], data[2])
        return self.line(grade)

    def curriculum(self, data):
        course = '%s %s' % (data[0], data[1])
        return self.line(course)

    def roster(self, data):
        student = '%s %s %s' % (data[0], data[1], data[2])
        return self.line(student)

    def section_header(self, data):
        return self.line(len(data))

    def line(self, data):
        return str(data) + "\n"
